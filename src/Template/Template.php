<?php

namespace Sda\Logowanie\Template;

class Template {

	public function show()
	{
		$template = $this->loadTemplate('/index.tmpl.html');
//		$template = str_replace('myfile.csv', $fileName, $template);
//		$template = str_replace('[[wszystkie]]', $rowCorrect + $rowIncorrect, $template);
//		$template = str_replace('[[poprawne]]', $rowCorrect, $template);
//		$template = str_replace('[[bledne]]', $rowIncorrect, $template);

		echo $template;

	}

//	public function showNoFile()
//	{
//		$template = $this->loadTemplate('/index.tmpl.html');
//		$template = str_replace('myfile.csv', 'Brak pliku', $template);
//		$template = str_replace('Brak rekordów','Brak pliku', $template);
//		echo $template;
//
//	}

	private function loadTemplate($templateName){
		return file_get_contents(__DIR__ . $templateName);
	}
}