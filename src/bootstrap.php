<?php
session_start();

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Sda\Logowanie\Controller\Controller;

require_once __DIR__ . '/../vendor/autoload.php';

$config = new Configuration();

$connectionParams = [
    'dbname' => 'loginform',
    'user' => 'root',
    'password' => '',
    'host' => 'localhost',
    'driver' => 'pdo_mysql',
];

$dbh = DriverManager::getConnection($connectionParams, $config);

//$template = new Template();
//$template->show();
$app = new Controller(
    $dbh
);
$app->run();
